#ifndef DBCANALYSISMODEL_H
#define DBCANALYSISMODEL_H

#include <QObject>
#include <QList>
#include "dbcmessagebase.h"

class DbcAnalysisModel : public QObject
{
    Q_OBJECT

private:


public:
    explicit DbcAnalysisModel(QObject *parent = 0);
    ~DbcAnalysisModel();

    QString Name;
    quint32 BaudRate;
    qint16 Address;

    QList<DbcMessageBase> MsgList;

    bool GetList(QString filename);

signals:

public slots:
};

#endif // DBCANALYSISMODEL_H
