#include "candatabase.h"

CanDataBase::CanDataBase()
{
    IsSend = false;
    IsExtendFrame = false;
    IsRemoteFrame = false;
}

void CanDataBase::SetTime(QDateTime time)
{
    this->Time = time;
}

void CanDataBase::SetTime(quint64 time)
{
    this->sTime = time / 1000000.0;
}

void CanDataBase::SetIsSend(bool send)
{
    this->IsSend = send;

}

void CanDataBase::SetData(uint id,unsigned char * data,int len,bool extend,bool remote)
{
    this->Id = id;
    this->Data =data;
    this->IsExtendFrame = extend;
    this->IsRemoteFrame = remote;
    this->Len = len;
}

void CanDataBase::GetStringData()
{
    QString str ;

    for(int i=0;i<this->Len;i++)
    {
        str += QString("%1 ").arg( this->Data[i],2,16,QLatin1Char('0'));
    }

    this->StrData = str;

   // return str;
}

