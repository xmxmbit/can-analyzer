#ifndef CANDATABASE_H
#define CANDATABASE_H

#include <QObject>
#include <QDateTime>

class CanDataBase
{
public:
    CanDataBase();

    QDateTime Time;

    double sTime;

    uint Id;

   unsigned char *Data;

    QString StrData;

    int Len;

    bool IsSend;

    bool IsExtendFrame;

    bool IsRemoteFrame;

    void SetTime(QDateTime time);
    void SetTime(quint64 time);

    void SetIsSend(bool send);

    void SetData(uint id,unsigned char * data,int len,bool extend = false,bool remote = false);

    void GetStringData();


};

#endif // CANDATABASE_H
