#include "dbcanalysismodel.h"
#include <QFile>
#include <QFileInfo>
#include <QDebug>
#include <QTextCodec>
#include <QRegExp>


DbcAnalysisModel::DbcAnalysisModel(QObject *parent) : QObject(parent)
{
    // MsgList = new QList<DbcMessageBase>;

}

DbcAnalysisModel::~DbcAnalysisModel()
{
    // delete MsgList;
}

bool DbcAnalysisModel::GetList(QString filename)
{
    QFile file(filename);

    file.open(QIODevice::ReadOnly|QIODevice::Text);

    MsgList.clear();

    while(!file.atEnd())
    {
        QByteArray line = file.readLine();
        QString str1 = QTextCodec::codecForName("GBK")->toUnicode(line);

        // qDebug()<< str1;

        if (str1.length() < 7)
            continue;
        if (str1.mid(0, 4) == "BU_:")
        {
            QStringList strs1 = str1.split(" ");
            if (strs1.length() == 4)
            {
                this->Name = strs1[1];
                this->BaudRate = strs1[2].toInt();
                this->Address = strs1[3].toInt(Q_NULLPTR,16);
            }
            else
            {
                this->Name = strs1[1];
                this->BaudRate = 250;
                this->Address = 0xE5;
            }
        }
        else if (str1.mid(0, 4) == "BO_ ")
        {
            QStringList strs1 = str1.trimmed().split(" ");
            DbcMessageBase fdb;

            fdb.Can.Id = strs1[1].toULong() & 0x1FFFFFFF;
            if (fdb.Can.Id > 0x7FF)
                fdb.Can.IsExtendFrame = true;

            fdb.Name = strs1[2].mid(0, strs1[2].length() - 1);
            //fdb.SendCycle = 0;

            if (strs1[strs1.length() - 1] != "Vector__XXX")
            {
                fdb.SendNodeName = strs1[strs1.length() - 1];
            }

            this->MsgList.append(fdb);
        }
        else if ((str1.mid(0, 5) == " SG_ ") || (str1.mid(0, 5) == "  SG_"))
        {
            dbcsingalbase dtb ;

            int index1 = str1.indexOf("SG_");
            int index2 = str1.indexOf(":");

            QString name = str1.mid(index1 + 3, index2 - index1 - 3).trimmed();
            dtb.Name = name;

            QStringList strs1 = str1.mid(index2 + 1, str1.length() - index2 - 1).split(" ");

            int i1 = strs1[1].indexOf('|');
            int i2 = strs1[1].indexOf('@');
            int start = (strs1[1].mid(0, i1).toInt());
            int number = (strs1[1].mid(i1 + 1, i2 - i1 - 1).toInt());
            int format = (strs1[1].mid(i2 + 1, 1).toInt());
            QString vtype = strs1[1].mid(i2 + 2, 1);

            if (vtype == "-")
                dtb.isSigned = true;

            dtb.Number = number;
            if (format == 1)
            {
                dtb.Start = start;
                dtb.isMoto = false;
            }
            else
            {
                int offset = 0;
                int end = 0;
                for (int j = 0; j < number; j++)
                {
                    end = start - j;
                    if ((end % 8 == 0) && (j != number - 1))
                        offset += 16;
                    end += offset;
                }

                dtb.Start = end;
                dtb.isMoto = true;
            }

            dtb.GetEnd();

            MsgList.back().SetMoto( dtb.isMoto);

            QStringList strs2 = strs1[2].replace("(", "").replace(")", "").split(",");
            dtb.Rate = strs2[0].toDouble();
            dtb.Offset = strs2[1].toDouble();

            QStringList strs3 = strs1[3].replace("[", "").replace("]", "").split("|");
            dtb.MinValue = (strs3[0].toDouble());
            dtb.MaxValue = (strs3[1].toDouble());

            if (dtb.MinValue == 0 && dtb.MinValue == dtb.MaxValue)
            {
                dtb.GetMaxMinValue();
            }

            dtb.Unit = strs1[4].replace("\"", "");

            MsgList.back().SingalList.append(dtb);
        }
        else if (str1.mid(0, 5) == "VAL_ ")
        {
            QStringList strs1 = str1.split(" ");
            uint id = (strs1[1].toULong()) & 0x1FFFFFFF;

            for (int i = 0;i<MsgList.size();i++)
            {
                DbcMessageBase item = MsgList.at(i);
                if (item.Can.Id == id)
                {
                    for(int j = 0;j< item.SingalList.size();j++)
                    {
                        dbcsingalbase dtb = item.SingalList.at(j);
                        if ( dtb.Name == strs1[2])
                        {
                            MsgList[i].SingalList[j].isEnum = true;

                            str1 += " "" "" """;

                            while (true)
                            {
                                int index1 = str1.indexOf('"');
                                if (index1 < 0)
                                    break;
                                int index2 = str1.indexOf('"', index1 + 1);
                                if (index2 < 0)
                                    break;

                                int  v = str1.mid(0, index1).toInt();
                                QString n = str1.mid(index1 + 1, index2 - index1 - 1).trimmed();

                              //  qDebug()<<v<<" "<<n;

                                MsgList[i].SingalList[j].Status.insert(v,n);

                                str1 = str1.mid(index2 + 1, str1.length() - index2 - 1);
                            }

                            break;
                        }
                    }
                    break;
                }
            }
        }
        else if (str1.mid(0, 4) == "CM_ ")
        {
            QStringList strs1 = str1.split(" ");
            if (strs1[1] == "SG_" && strs1.length() >= 4)
            {
                uint id = (strs1[2].toULong()) & 0x1FFFFFFF;

                for (int i = 0;i<this->MsgList.size();i++)
                {
                    DbcMessageBase item = this->MsgList.at(i);
                    if (item.Can.Id == id)
                    {
                        for(int j = 0;j< item.SingalList.count();j++)
                        {
                            dbcsingalbase dtb = item.SingalList.at(j);

                            if (dtb.Name == strs1[3])
                            {
                                int index = str1.indexOf("\"");

                                this->MsgList[i].SingalList[j].Note =  str1.mid(index + 1, str1.length() - index - 4).replace(",", " ");
                            }
                        }
                    }
                }
            }
            else if (strs1.length() == 2)
            {
                this->Name = strs1[1].replace("\"", "").replace("@", "").replace(";", "");
            }
        }
        else if (str1.mid(0, 4) == "BA_ ")
        {
            QStringList strs1 = str1.split(" ");

            if (strs1[2] == "BO_")
            {
                uint id = (strs1[3].toULong()) & 0x1FFFFFFF;

                for (int i = 0;i<this->MsgList.size();i++)
                {
                    DbcMessageBase item = this->MsgList.at(i);
                    if (item.Can.Id == id)
                    {
                        if (strs1[1] == "\"GenMsgCycleTime\"")
                        {
                            int time = (strs1[4].replace(";", "").toInt());
                            this->MsgList[i].SendCycle = time;
                            break;
                        }
                    }
                }
            }
        }
    }


    file.close();

    qDebug()<<this->MsgList.size();

    return true;
}



