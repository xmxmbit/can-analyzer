#ifndef DBCMESSAGEBASE_H
#define DBCMESSAGEBASE_H

#include <QObject>
#include <QList>
#include "dbcsingalbase.h"
#include "candatabase.h"

class DbcMessageBase
{

public:
    DbcMessageBase();

    QList<dbcsingalbase> SingalList;

    QString Name;

    bool IsSend;

    quint32 SendCycle;

    QString SendNodeName;

    CanDataBase Can;

    bool isMoto;

    void SetMoto(bool moto);

    void getValue(const unsigned char *Data,int len);

};

#endif // DBCMESSAGEBASE_H
