#include "drawline.h"
#include <QDebug>
#include <QPainterPath>

DrawLine::DrawLine(QWidget *parent) : QWidget(parent)
{
    this->xAxis.max = 30;
    this->xAxis.min = 0;
    this->xAxis.interval = 10;
    this->xAxis.yLocation = 30;
    this->xAxis.xLocation = 60;

}

DrawLine::~DrawLine()
{
    //  delete xAxis;
}

void DrawLine::getRealValue(int i,int j,double x, double y)
{
    for(int k = 0;k<Lines.size();k++)
    {
        if(Lines[k].lineI == i && Lines[k].lineJ == j)
        {
            if(x > xAxis.max)
            {
                double dd = x-xAxis.max;
                xAxis.min += dd;
                xAxis.max = x;

                updataPoint();
            }

            AxisModel yAxis = Lines[k].yAxis;
            double xx = xAxis.startPoint.x() + (x - xAxis.min) * (xAxis.endPoint.x() - this->xAxis.startPoint.x()) / (xAxis.max - xAxis.min) ;
            double yy = yAxis.startPoint.y() + (yAxis.max - y) * (yAxis.endPoint.y() - yAxis.startPoint.y()) / (yAxis.max - yAxis.min) ;

            Lines[k].addReal(x,y);
            Lines[k].add(xx,yy);

           // qDebug()<<"实际值："<<x<<","<<y;
           // qDebug()<<"计算值："<<xx<<","<<yy;

            update();
            break;
        }
    }
}

void DrawLine::getxAxisPoint()
{
    this->xAxis.startPoint = QPointF(this->xAxis.xLocation , this->height() - this->xAxis.yLocation);
    this->xAxis.endPoint = QPointF(this->width()-6 , this->height() - this->xAxis.yLocation);
}

void DrawLine::updataPoint()
{
    for(int k = 0;k<Lines.size();k++)
    {
        Lines[k].polygon.clear();

        for(int p=0;p< Lines[k].Points.size();p++)
        {
            if(Lines[k].Points[p].x()>=xAxis.min && Lines[k].Points[p].x()<= xAxis.max )
            {
                AxisModel yAxis = Lines[k].yAxis;
                double x1 = xAxis.startPoint.x() + (Lines[k].Points[p].x() - xAxis.min) * (xAxis.endPoint.x() - this->xAxis.startPoint.x()) / (xAxis.max - xAxis.min) ;
                double y1 = yAxis.startPoint.y() + (yAxis.max - Lines[k].Points[p].y()) * (yAxis.endPoint.y() - yAxis.startPoint.y()) / (yAxis.max - yAxis.min) ;
                Lines[k].add(x1,y1);
            }
        }
    }
}



void DrawLine::paintEvent(QPaintEvent *event)
{
    getxAxisPoint();
    QPainter p(this) ;
    p.drawLine(xAxis.startPoint,xAxis.endPoint);

    for(int i = 0;i<=xAxis.interval;i++)
    {
        double d =xAxis.startPoint.x() +  i * (xAxis.endPoint.x() - xAxis.startPoint.x()) / xAxis.interval;
        QString lb = QString("%1").arg(xAxis.min + i * (xAxis.max - xAxis.min) / xAxis.interval,0,'f',1);
        p.drawLine(QPointF(d,xAxis.startPoint.y()),QPointF(d,xAxis.startPoint.y() + 10) );
        p.drawText(QPointF(d-6,xAxis.startPoint.y() + 20),lb);
        //qDebug()<<"标签："<<xAxis.max<<","<<lb;
    }

    for(int i = 0;i<this->Lines.size();i++)
    {
        double num = this->Lines.size();
        double d = (this->height() - this->xAxis.yLocation - 10) / num;

        this->Lines[i].yAxis.startPoint=QPointF(this->xAxis.xLocation - 6 , 10 + i * d);
        this->Lines[i].yAxis.endPoint=QPointF(this->width() - 6  , 10 + i * d + d - 10);

        p.drawLine(this->Lines[i].yAxis.startPoint,QPointF(this->Lines[i].yAxis.startPoint.x(),this->Lines[i].yAxis.endPoint.y()));

        p.drawLine(this->Lines[i].yAxis.startPoint,QPointF(this->Lines[i].yAxis.startPoint.x()-10 ,this->Lines[i].yAxis.startPoint.y()));
        p.drawLine(QPointF(this->Lines[i].yAxis.startPoint.x() ,this->Lines[i].yAxis.endPoint.y()),QPointF(this->Lines[i].yAxis.startPoint.x()-10 ,this->Lines[i].yAxis.endPoint.y()));

        QString max = QString("%1").arg(this->Lines[i].yAxis.max,0,'f',1);
        p.drawText(QPointF(this->Lines[i].yAxis.startPoint.x() - 54,this->Lines[i].yAxis.startPoint.y() + 5),max);
        QString min = QString("%1").arg(this->Lines[i].yAxis.min,0,'f',1);
        p.drawText(QPointF(this->Lines[i].yAxis.startPoint.x() - 54,this->Lines[i].yAxis.endPoint.y() + 5),min);

        QString name = this->Lines[i].name;
        double y =( this->Lines[i].yAxis.startPoint.y() + this->Lines[i].yAxis.endPoint.y()) / 2.0;
        p.drawText(QPointF(this->Lines[i].yAxis.startPoint.x() - 54,y),name);

     //   qDebug()<<this->Lines[i].polygon.size();
        if(this->Lines[i].polygon.size()>2)
        {
            p.drawPolyline(this->Lines[i].polygon);
        }
    }

    p.end();
}
