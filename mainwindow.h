#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "cansetmodel.h"
#include <QThread>
#include "dbcanalysismodel.h"
#include <QTreeWidget>

#include <QStandardItem>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_btnSend_clicked();

    void on_btnConnect_clicked();

    void CloseWin();

    void UpdataReceive(CanDataBase * cdb);

    void on_btnOpenDbc_clicked();

    void on_tbMsg_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);

    void on_trwLine_itemDoubleClicked(QTreeWidgetItem *item, int column);

signals:
    void StartDataReceive();

private:
    Ui::MainWindow *ui;

    CanSetModel *CanSet;

    QThread *myThread;

    QString BaudRate;

    DbcAnalysisModel *Dbc;

    int selectRow;

    void getTreeView();

    QStandardItemModel *model;


};

#endif // MAINWINDOW_H
