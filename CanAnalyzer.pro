#-------------------------------------------------
#
# Project created by QtCreator 2021-03-27T09:47:22
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CanAnalyzer
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    candatabase.cpp \
    cansetmodel.cpp \
    dbcmessagebase.cpp \
    dbcsingalbase.cpp \
    dbcanalysismodel.cpp \
    drawline.cpp \
    linebase.cpp \
    axismodel.cpp

HEADERS  += mainwindow.h \
    CANApi/canframe.h \
    CANApi/config.h \
    CANApi/typedef.h \
    CANApi/zlgcan.h \
    candatabase.h \
    cansetmodel.h \
    dbcmessagebase.h \
    dbcsingalbase.h \
    dbcanalysismodel.h \
    drawline.h \
    linebase.h \
    axismodel.h

FORMS    += mainwindow.ui


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/CANApi/ -lzlgcan
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/CANApi/ -lzlgcan

INCLUDEPATH += $$PWD/CANApi
DEPENDPATH += $$PWD/CANApi

INCLUDEPATH += $$PWD/CANApi
DEPENDPATH += $$PWD/CANApi
