#ifndef AXISMODEL_H
#define AXISMODEL_H

#include <QPoint>
#include <QPainter>
#include <QMap>

class AxisModel
{
public:
    AxisModel();

    QPointF startPoint;
    QPointF endPoint;

    double max;
    double min;

    double interval ;

    double xLocation;
    double yLocation;


    QMap<double,QString> Lables;
};

#endif // AXISMODEL_H
