#ifndef DBCSINGALBASE_H
#define DBCSINGALBASE_H

#include <QObject>
#include <QMap>

class dbcsingalbase
{
public:
    dbcsingalbase();

    QString Name;   //名称
    int Start;      //启始位
    int Number;     //位长度
    int End;        // 结束位
    double Rate;    //分辩率
    double Offset;  //偏移
    QString Unit;   //单位
    QString Note;   //备注
    double MaxValue;    //最小值
    double MinValue;    //最大值
    double Value;       //实际值
    QString StrValue;   //显示值
    bool isSigned;        //有符号
    bool isMoto;        //是否摩托罗拉格式
    bool isEnum;      //

    quint8 multiplexer_type;    //see 'multiplexer type' above
    quint8 valType;            //0:integer, 1:float, 2:double
    quint32 multiplexer_value;

    QMap<int,QString> Status;


    void GetMaxMinValue();
    void GetEnd();
    double GetValue();
    QString GetStringValue();
    QString GetStringValue(int value);
};

#endif // DBCSINGALBASE_H
