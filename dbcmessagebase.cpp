#include "dbcmessagebase.h"
#include "qmath.h"
#include <QDebug>

DbcMessageBase::DbcMessageBase()
{
    //  this->SingalList = new QList<dbcsingalbase>;

    // this->Can = new CanDataBase();

    this->IsSend = false;

    SendCycle = 0;
}


void DbcMessageBase::SetMoto(bool moto)
{
    this->isMoto = moto;
}

void DbcMessageBase::getValue(const unsigned char *data, int len)
{
    QString bits;

    for(int i=0;i<len;i++)
    {
        QString str = QString("%1").arg(data[i],8,2,QLatin1Char('0'));
        QString str1;
        str1.fill('0',str.size());
        for(int j = 0;j<str1.length();j++)
            str1[j] = str[str1.length()-j-1];
        bits += str1;
    }

    for(int j=0;j<SingalList.size();j++)
    {
        dbcsingalbase item = SingalList.at(j);

        qint32 value = 0;
        bool isGetValue = true;

        if (isMoto == false)
        {
            for (int i = 0; i < item.Number; i++)
            {
                int p = i + item.Start;
                if (p >= 0 && p < len * 8)
                {
                    if (bits[i + item.Start] == QLatin1Char('1'))
                        value += (qint32)qPow(2, i);
                }
                else
                {
                    isGetValue = false;
                    break;
                }
            }
        }
        else
        {
            int offset = 0;
            for (int i = 0; i < item.Number; i++)
            {
                int p = i + item.Start;
                if ((p % 8 == 0) && (i != 0))
                    offset -= 16;
                p += offset;

                if (p >= 0 && p < len * 8)
                {
                    if (bits[p] == QLatin1Char('1'))
                        value += (qint32)qPow(2, i);
                }
                else
                {
                    isGetValue = false;
                    break;
                }
            }
        }

        if (item.isSigned)
        {
            qint32 vv = qPow(2, item.Number);

            if (value >= vv / 2) //负数
            {
                value = value - vv;
            }
        }

        if (isGetValue)
        {
           //  qDebug()<< value;
            SingalList[j].Value = value;
        }
    }
}



