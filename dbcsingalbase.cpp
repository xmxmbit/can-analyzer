#include "dbcsingalbase.h"
#include "qmath.h"

dbcsingalbase::dbcsingalbase()
{
    this->isSigned = false;
    this->isMoto = false;
    this->isEnum = false;
    this->Value = 0;
}

void dbcsingalbase::GetMaxMinValue()
{

}

void dbcsingalbase::GetEnd()
{
    if(isMoto)
    {
        this->End = Start + Number - 1;
    }
    else
    {
        int offset = 0;
        int end = 0;
        for (int i = 0; i < Number; i++)
        {
            end = i + Start;
            if ((end % 8 == 0) && (i != 0))
                offset -= 16;
            end += offset;
        }

        if (end < 0)
            end = 0;

        this->End=end;

    }
}

double dbcsingalbase::GetValue()
{
    double value = (Value * Rate + Offset);
    return value;
}

QString dbcsingalbase::GetStringValue()
{
    if (this->isEnum)
    {
        QString str1 = "";

        if(Status.contains(Value))
        {
            return Status[Value];
        }
        else
        {
            return "无效值";
        }
    }
    else
    {
        double len = 0;
        double value = (Value * Rate + Offset);
        if (value == 0)
            len = 1;
        else
        {
            if (Rate >= 1)
                len = 1;
            else
                len = ceil(qLn(0.1)/qLn(Rate)) + 1;
        }
        QString str = QString::number(value,'f',5);
        int point = str.indexOf(".");

        if (len == 1)
            return str.mid(0, point) + Unit;
        else
            return str.mid(0, point + (int)len) + Unit;
    }
}

QString dbcsingalbase::GetStringValue(int value)
{
    Value = value;
    GetStringValue();
}
