#ifndef CANSETMODEL_H
#define CANSETMODEL_H

#include <QObject>

#include "canapi/zlgcan.h"
#include "canapi/typedef.h"
#include "canapi/config.h"
#include "canapi/canframe.h"
#include "candatabase.h"

class CanSetModel : public QObject
{
    Q_OBJECT
public:
    explicit CanSetModel(QObject *parent = 0);

    CHANNEL_HANDLE chHandle;

    DEVICE_HANDLE dhandle;

    int DeviceType;

    int DeveiceIndex;

    char* BaudRate;

    char* BaudRateFd;


    bool isStart;

    void SetPara(int type,int index,char* br);


    bool CanConnect();

    void CanClose();

    bool CanSend(CanDataBase *data);

    void DataReceive();

    void AddData(const ZCAN_Receive_Data *data, UINT len);

    void AddData(const ZCAN_ReceiveFD_Data *data, UINT len);

    void AddData(CanDataBase *data);

signals:

    void DataUpdata(CanDataBase *data);

public slots:
};

#endif // CANSETMODEL_H
