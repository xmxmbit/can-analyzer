#ifndef LINEBASE_H
#define LINEBASE_H

#include <QPoint>
#include <QPainter>
#include "axismodel.h"
#include <QPolygon>
#include <QList>



class LineBase
{
public:
    LineBase();

    int lineI;
    int lineJ;

    AxisModel yAxis;

    QString name;

     QList<QPointF> Points;

     QPolygonF polygon;

     void add(double x,double y);
     void addReal(double x,double y);
};

#endif // LINEBASE_H
