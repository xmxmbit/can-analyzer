#ifndef DRAWLINE_H
#define DRAWLINE_H

#include <QWidget>
#include <QPainter>
#include <QPen>
#include "axismodel.h"
#include <QPoint>
#include <QList>
#include "linebase.h"

class DrawLine : public QWidget
{
    Q_OBJECT
public:
    explicit DrawLine(QWidget *parent = 0);
        ~DrawLine();

    QList<LineBase> Lines;

    void getRealValue(int i,int j,double x, double y);
    void getxAxisPoint();
    void updataPoint();

signals:

public slots:

private:

    AxisModel xAxis;



protected:
    void paintEvent(QPaintEvent *event);
};

#endif // DRAWLINE_H
